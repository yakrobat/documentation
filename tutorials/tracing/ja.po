msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2020-01-12 18:05+0100\n"
"PO-Revision-Date: 2013-03-28 19:02+0900\n"
"Last-Translator: Masato Hashimoto <cabezon.hashimoto@gmail.com>\n"
"Language-Team: Japanese <inkscape-translator@lists.sourceforge.net>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Japanese\n"

#: tracing-f06.svg:49(format) tracing-f05.svg:49(format)
#: tracing-f04.svg:49(format) tracing-f03.svg:49(format)
#: tracing-f02.svg:49(format) tracing-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tracing-f06.svg:70(tspan) tracing-f05.svg:70(tspan)
#: tracing-f04.svg:70(tspan) tracing-f03.svg:70(tspan)
#: tracing-f02.svg:70(tspan)
#, no-wrap
msgid "Original Image"
msgstr "オリジナル画像"

#: tracing-f06.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "トレースイメージ / 出力パス - 簡略化"

#: tracing-f06.svg:86(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 ノード)"

#: tracing-f05.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "トレースイメージ / 出力パス"

#: tracing-f05.svg:86(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1,551 ノード)"

#: tracing-f04.svg:81(tspan) tracing-f04.svg:97(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "量子化 (12 色)"

#: tracing-f04.svg:86(tspan) tracing-f03.svg:86(tspan)
#: tracing-f02.svg:86(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr "フィルあり、ストロークなし"

#: tracing-f04.svg:102(tspan) tracing-f03.svg:102(tspan)
#: tracing-f02.svg:102(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr "ストロークあり、フィルなし"

#: tracing-f03.svg:81(tspan) tracing-f03.svg:97(tspan)
#, no-wrap
msgid "Edge Detected"
msgstr "エッジ検出"

#: tracing-f02.svg:81(tspan) tracing-f02.svg:97(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr "明るさの境界 しきい値"

#: tracing-f01.svg:70(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "トレースダイアログの主なオプション"

#: tutorial-tracing.xml:6(title)
#, fuzzy
msgid "Tracing bitmaps"
msgstr "トレース"

#: tutorial-tracing.xml:10(para)
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Inkscape の特徴の一つにビットマップをトレースして &lt;パス&gt; 要素を作成する"
"ツールがあります。このチュートリアルはこのツールがどう動くかを知る助けになる"
"はずです。"

#: tutorial-tracing.xml:17(para)
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"現在 Inkscape はビットマップトレースエンジンに Peter Selinger が制作した "
"Potrace (<ulink url=\"http://potrace.sourceforge.net\">potrace.sourceforge."
"net</ulink>) を使用しています。将来的には他のトレーシングエンジンも使えるよう"
"になると思いますが、今のところ、この素晴らしいツールは我々にとって必要にして"
"十分です。"

#: tutorial-tracing.xml:24(para)
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"トレースの目的は、オリジナルの画像を忠実に再現することではなく、最終的な完成"
"品を作成することでもないということを覚えておいてください。自動トレーサーにそ"
"んなことはできません。トレースがすることは、あなたが描く作品の一つの資源とし"
"ての一連の曲線を提供することなのです。"

#: tutorial-tracing.xml:31(para)
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"Potrace は白黒のビットマップを解釈し、曲線のセットを作成します。入力画像を "
"Potrace が使用できるようにするため、現在 3 タイプのフィルターが用意されていま"
"す。"

#: tutorial-tracing.xml:37(para)
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"ほとんどの場合、Potrace は中間像のビットマップの色の濃いピクセルをより多くト"
"レースします。トレース量が増えると、処理にかかる CPU 時間が増え、&lt;パス"
"&gt; 要素は非常に多くなります。まずより色の薄い中間像で実験し、徐々に濃くして"
"希望する割合や出力パスの複雑さを決めていくことをおすすめします。"

#: tutorial-tracing.xml:45(para)
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"トレーサーを使うには、画像を読み込むかインポートし、<command>パス &gt; ビット"
"マップをトレース</command> アイテムを選ぶか、<keycap>Shift+Alt+B</keycap> を"
"押します。"

#: tutorial-tracing.xml:57(para)
msgid "The user will see the three filter options available:"
msgstr "3 つのフィルターオプションが見えるでしょう:"

#: tutorial-tracing.xml:62(para)
#, fuzzy
msgid "Brightness Cutoff"
msgstr "明るさの境界 しきい値"

#: tutorial-tracing.xml:67(para)
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"これは単にピクセルの赤、青、および緑 (またはグレーのシェード) の合計を黒ある"
"いは白とみなす指標として使用しているだけです。しきい値は 0.0 (黒) から 1.0 "
"(白) が設定できます。高いしきい値を設定すると、少数のピクセルだけが「白」とみ"
"なされ、中間像は濃くなります。"

#: tutorial-tracing.xml:83(para)
#, fuzzy
msgid "Edge Detection"
msgstr "エッジ検出"

#: tutorial-tracing.xml:88(para)
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"これは類似するコントラストの平衡線を素早く検出する手段として、J. Canny が考案"
"したエッジ検出アルゴリズムを使用しています。これは明るさのしきい値の結果より"
"もオリジナルの情報を残さない中間像を生成しますが、おそらく他では無視される曲"
"線情報を提供します。ここでのしきい値の設定 (0.0 – 1.0) は出力に含むコントラス"
"トのエッジに隣接したピクセルを調節します。この設定で出力のエッジの暗さや太さ"
"を調整できます。"

#: tutorial-tracing.xml:106(para)
msgid "Color Quantization"
msgstr "色の量子化"

#: tutorial-tracing.xml:111(para)
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"このフィルターの結果は他の 2 つよりも大きく異なる中間像を生成しますが、実は非"
"常に役に立ちます。これは明るさやコントラストの平衡線の代わりに、明るさやコン"
"トラストが同じであっても色が変わる境界を検出します。ここでは、中間像がカラー"
"の場合に出力する色数を決定します。これが色が偶数か奇数かで白/黒を決定します。"

#: tutorial-tracing.xml:127(para)
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"これら 3 つのフィルターを試してみて、さまざまタイプの入力画像でさまざまなタイ"
"プの出力を見比べてください。"

#: tutorial-tracing.xml:133(para)
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"トレース後、出力パスに対して <command>パス &gt; パスの簡略化</command> "
"(<keycap>Ctrl+L</keycap>) を行ってノード数を整理することをおすすめします。こ"
"れは Potrace の出力をより編集しやすくします。例えば、ここにギターを弾く老人の"
"典型的なトレースがあります:"

#: tutorial-tracing.xml:147(para)
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"パスには厖大な数のノードがあることに注目してください。<keycap>Ctrl+L</"
"keycap> を押したあとの典型的な結果がこれです:"

#: tutorial-tracing.xml:159(para)
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"描写はすこしだけ似ていてラフですが、描画はよりシンプルになり、編集しやすく"
"なっています。トレースは画像を忠実に描写するのではなく、あなたが作品を描画す"
"る中で使用する一連の曲線を作成するものであることを忘れないでください。"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr "Masato Hashimoto <cabezon.hashimoto@gmail.com>, 2009, 2012."

#~ msgid "Optimal Edge Detection"
#~ msgstr "最適なエッジ検出"
